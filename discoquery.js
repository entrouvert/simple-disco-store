(function () {
    var C_ENTITY_ID = "entityID";
    var C_RETURN = "return";
    var C_POLICY = "policy";
    var C_RETURNIDPARAM = "returnIDParam";
    var C_IS_PASSIVE = "isPassive";
    var C_IDP_ENTITY_ID = "IdPentityID";
    var C_STORE = "store";
    var COOKIE_TIMEOUT_DAYS = 730;

    $.disco_query = function (entity_id, url, response_url, callback) {
      var iframe = $('<iframe style="display: none"/>');
      var query_url = url;
      if (url.indexOf('?') == -1) {
        query_url = query_url + '?';
      } else {
        query_url = query_url + '&';
      }
      var param = {};
      param[C_ENTITY_ID] = entity_id;
      param[C_RETURN] = response_url + '?' + C_STORE + '=' + escape(url);
      param[C_IS_PASSIVE] = 'true';
      query_url = query_url + $.param(param);
      debug_alert('query_url '+query_url);
      iframe.attr('src', query_url);
      if (callback) {
        var disco_callback = function (entity_id, store, auth) {
          callback(entity_id, store, auth);
          $(iframe).remove();
        };
        window.disco_callback = disco_callback;
      } else {
        window.disco_callback = undefined;
      }
      $(iframe).appendTo('body');
    }
    $.disco_set = function (entity_id, idp_entity_id, url, response_url, callback) {
      var iframe = $('<iframe style="display: none"/>');
      var query_url = url;
      if (url.indexOf('?') == -1) {
        query_url = query_url + '?';
      } else {
        query_url = query_url + '&';
      }
      var param = {};
      param[C_ENTITY_ID] = entity_id;
      param[C_RETURN] = response_url + '?' + C_STORE + '=' + escape(url);
      param[C_IDP_ENTITY_ID] = idp_entity_id;
      query_url = query_url + $.param(param);
      debug_alert('query_url '+query_url);
      iframe.attr('src', query_url);
      if (callback) {
        var disco_callback = function (entity_id, store, auth) {
          callback(entity_id, store, auth);
          $(iframe).remove();
        };
        window.disco_callback = disco_callback;
      } else {
        window.disco_callback = undefined;
      }
      $(iframe).appendTo('body');
    }
})();
