Simple IdP Discovery Store and Query
====================================

How to use
----------

Copy this code in your web app.

Place the response.html somewhere, let's say at http://sp.example.com/response.html.

Add this in your page header:

  <script type="text/javascript" src="jquery-1.7.2.min.js"></script>
  <script type="text/javascript" src="jquery.qs.js"></script>
  <script type="text/javascript" src="discoquery.js"></script>
  <script type="text/javascript" src="debug.js"></script>

And make the linked files accessible.

Call this to get the globally stored preference:

  $.disco_query("http://sp.example.com/metadata", // requestor ID
      "http://disco.example.com/store", // store URL
      "http://sp.example.com/response.html",  // response.html URL
      function (entity_id, store, auth) { // callback if a response is received
        // do something with it
        // entity_id: 
        //    is the entity ID of the preferred IdP of the user
        // store: 
        //    is the URL of the store answering (for example if you launch
        //    many request, you can know which store answered
        // auth:
        //    should be "saml" but could be anything else (facebook, openid, etc..)
  });

Call this to store your preference (you can launch many store request at the same time):

  $.disco_set("http://sp.example.com/metadata", // requestor ID
      "http://chosen_idp.example.com/metadata", // store URL
      "http://disco.example.com/store", // idp IDP to store
      "http://sp.example.com/response.html", // response.html URL
      function (entity_id, store, auth) {  // callback if you want to know that the saving succeeded
         // do what you want here
      });

How it works
------------

Request to the discovery services are made inside iframes. So in order for the
request to be received, you need to let the iframe load itself. It means that
if you are doing those calls when leaving the current page, you must wait a
certain time for the loading to occur (and otherwise timeout using setTimeout).
